package com.huntfalcon.api.config;

import com.huntfalcon.api.remoteapi.RemoteRpc;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.support.WebClientAdapter;
import org.springframework.web.service.invoker.HttpServiceProxyFactory;

import java.time.Duration;

@Configuration
public class ClientConfig {

    @Bean
    RemoteRpc remoteRpc() {
        WebClient client = WebClient.builder().baseUrl("https://v0.yiketianqi.com").build();
        HttpServiceProxyFactory factory = HttpServiceProxyFactory.builder(WebClientAdapter.forClient(client)).blockTimeout(Duration.ofSeconds(60)).build();
        return factory.createClient(RemoteRpc.class);
    }

}
