package com.huntfalcon.api.controller;

import com.huntfalcon.api.remoteapi.RemoteRpc;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @Resource
    private RemoteRpc remoteRpc;

    @GetMapping("/test")
    public String test() {
        return remoteRpc.demo();
    }
}
