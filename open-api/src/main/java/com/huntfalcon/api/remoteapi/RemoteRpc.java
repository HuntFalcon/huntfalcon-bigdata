package com.huntfalcon.api.remoteapi;

import org.springframework.web.service.annotation.GetExchange;
import org.springframework.web.service.annotation.HttpExchange;

@HttpExchange("/")
public interface RemoteRpc {

    @GetExchange("api?unescape=1&version=v91&appid=43656176&appsecret=I42og6Lm&ext=&cityid=&city=")
    String demo();

}
